FactoryGirl.define do
  factory :organisation do
    name "MyString"
    adress1 "MyString"
    adress2 "MyString"
    postal "MyString"
    city "MyString"
    state "MyString"
    country "MyString"
  end
end
