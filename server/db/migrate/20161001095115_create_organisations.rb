class CreateOrganisations < ActiveRecord::Migration[5.0]
  def change
    create_table :organisations do |t|
      t.string :name
      t.string :adress1
      t.string :adress2
      t.string :postal
      t.string :city
      t.string :state
      t.string :country

      t.timestamps
    end
  end
end
