class CreateUserOrganisations < ActiveRecord::Migration[5.0]
  def change
    create_table :user_organisations do |t|
      t.references :user, foreign_key: true
      t.references :organisation, foreign_key: true
      t.string :status

      t.timestamps
    end
  end
end
