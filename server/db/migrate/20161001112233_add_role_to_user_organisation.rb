class AddRoleToUserOrganisation < ActiveRecord::Migration[5.0]
  def change
  	add_column :user_organisations, :admin, :boolean
  end
end
