class CreateOrganisationProviders < ActiveRecord::Migration[5.0]
  def change
    create_table :organisation_providers do |t|
      t.references :organisation, foreign_key: true
      t.references :provider, foreign_key: true

      t.timestamps
    end
  end
end
