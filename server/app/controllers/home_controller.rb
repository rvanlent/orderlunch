class HomeController < ApplicationController
  def index
  	if signed_in?

  		@organisations = current_user.organisations
  		if @organisations.length == 1 
  			session[:organisation_id] = @organisations[0].id
  			redirect_to order_path 
  		else
	  		# more than one organisation and org ses var not set?
	  	  	render 'select_organisation'
    	end

  	else
    	render 'index'
  	end
  end

  def home
  	
  end

end
