json.extract! category, :id, :name, :provider_id, :created_at, :updated_at
json.url category_url(category, format: :json)