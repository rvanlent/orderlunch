class Organisation < ApplicationRecord
	has_many :user_organisations
    has_many :users, through: :user_organisations	

    has_many :organisation_providers
    has_many :providers, through: :organisation_providers	
end
