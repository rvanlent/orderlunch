class Provider < ApplicationRecord
	has_many :categories

	has_many :organisation_providers
    has_many :organisations, through: :organisation_providers	
end
