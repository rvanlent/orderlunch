class OrganisationProvider < ApplicationRecord
  belongs_to :organisation
  belongs_to :provider
end
