Rails.application.routes.draw do
	root to: 'home#index'
  
  resources :providers do
  	resources :categories
  end

  resources :organisations do
  	resources :providers
  end
  

  devise_for :users
  
  get 'order', to: 'organisations#order'
  get 'home', to: 'home#home'



  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
