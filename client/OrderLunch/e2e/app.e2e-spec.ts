import { OrderLunchPage } from './app.po';

describe('order-lunch App', function() {
  let page: OrderLunchPage;

  beforeEach(() => {
    page = new OrderLunchPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
